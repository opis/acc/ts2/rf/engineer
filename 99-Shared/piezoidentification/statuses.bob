<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-18 14:43:03 by iocuser-->
<display version="2.0.0">
  <name>Piezo Identification Status Details Chan $(CHAN) Cav $(CAVITY)</name>
  <width>690</width>
  <height>540</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <width>690</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Status_subtitle</name>
    <class>SUBTITLE</class>
    <text>Status Details - Cavity $(CAVITY) Channel $(CHAN)</text>
    <x>340</x>
    <y>20</y>
    <width>330</width>
    <height>30</height>
    <font>
      <font name="Header 2" family="Liberation Sans" style="BOLD" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>2</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="group" version="3.0.0">
    <name>IOCs_group</name>
    <x>20</x>
    <y>70</y>
    <width>650</width>
    <height>450</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <widget type="rectangle" version="2.0.0">
      <name>IOCs_rectangle</name>
      <width>650</width>
      <height>450</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>IOCs_title</name>
      <class>HEADER2</class>
      <text>IOCs:</text>
      <width>650</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Liberation Sans" style="BOLD" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Piezo_rectangle</name>
      <x>20</x>
      <y>40</y>
      <width>200</width>
      <height>290</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>5</corner_width>
      <corner_height>5</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Piezo_label</name>
      <text>Piezo:</text>
      <x>20</x>
      <y>40</y>
      <width>200</width>
      <height>30</height>
      <font>
        <font name="SUBSUB-GROUP-HEADER" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>Safety_LED</name>
      <pv_name>$(P)$(R)PiezoCh$(CHAN)SafetyStatus-RB</pv_name>
      <x>40</x>
      <y>80</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>Waveform_LED</name>
      <pv_name>$(P)$(R)PiezoCh$(CHAN)WavConfStatus-RB</pv_name>
      <x>40</x>
      <y>120</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>TriggerAct_LED</name>
      <pv_name>$(P)$(R)PiezoCh$(CHAN)TrgActStatus-RB</pv_name>
      <x>40</x>
      <y>160</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>TriggerSens_LED</name>
      <pv_name>$(P)$(R)PiezoCh$(CHAN)TrgSensStatus-RB</pv_name>
      <x>40</x>
      <y>200</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>SensorAcq_LED</name>
      <pv_name>$(P)$(R)PiezoCh$(CHAN)SensAcqStatus-RB</pv_name>
      <x>40</x>
      <y>240</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Safety_label</name>
      <text>Safety</text>
      <x>80</x>
      <y>80</y>
      <width>120</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Waveform_label</name>
      <text>Waveform Config</text>
      <x>80</x>
      <y>120</y>
      <width>120</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>TriggerAct_label</name>
      <text>Trigger Actuator</text>
      <x>80</x>
      <y>160</y>
      <width>120</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>TriggerSens_label</name>
      <text>Trigger Sensor</text>
      <x>80</x>
      <y>200</y>
      <width>110</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>SensorAcq_label</name>
      <text>Sensor Acquisition</text>
      <x>80</x>
      <y>240</y>
      <width>130</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>LLRF_rectangle</name>
      <x>240</x>
      <y>40</y>
      <width>190</width>
      <height>210</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>5</corner_width>
      <corner_height>5</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LLRF_label</name>
      <text>LLRF:</text>
      <x>240</x>
      <y>40</y>
      <width>190</width>
      <height>30</height>
      <font>
        <font name="SUBSUB-GROUP-HEADER" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>OpenLoop_LED</name>
      <pv_name>$(P)$(R)CheckLLRFStatusCh$(CHAN).A</pv_name>
      <x>260</x>
      <y>80</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>CavityAmp_LED</name>
      <pv_name>$(P)$(R)CheckCavityAmpCh$(CHAN)</pv_name>
      <x>260</x>
      <y>120</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>OpenLoop_label</name>
      <text>Open Loop</text>
      <x>300</x>
      <y>80</y>
      <width>110</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>CavityAmp_label</name>
      <text>Cavity Amp Level</text>
      <x>300</x>
      <y>120</y>
      <width>120</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>FSM_LED</name>
      <pv_name>$(P)$(R)CheckLLRFStatusCh$(CHAN).B</pv_name>
      <x>260</x>
      <y>160</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>FMS_label</name>
      <text>FSM</text>
      <x>300</x>
      <y>160</y>
      <width>110</width>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Cavity_rectangle</name>
      <x>450</x>
      <y>40</y>
      <width>180</width>
      <height>130</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>5</corner_width>
      <corner_height>5</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Cavity_label</name>
      <text>Cavity:</text>
      <x>450</x>
      <y>40</y>
      <width>180</width>
      <height>30</height>
      <font>
        <font name="SUBSUB-GROUP-HEADER" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Detu_label</name>
      <text>Detuning</text>
      <x>510</x>
      <y>80</y>
      <height>30</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>Detu_LED</name>
      <pv_name>$(P)$(R)DetuStatusCh$(CHAN)-RB</pv_name>
      <x>470</x>
      <y>80</y>
      <width>30</width>
      <height>30</height>
      <off_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </off_color>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>ADC_update</name>
      <pv_name>$(P)$(R)CalcAvgPiezoCh$(CHAN)Adc-RB</pv_name>
      <x>130</x>
      <y>280</y>
      <width>80</width>
      <height>30</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ADC_label</name>
      <text>ADC Voltage:</text>
      <x>30</x>
      <y>280</y>
      <width>90</width>
      <height>30</height>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Detu_update</name>
      <pv_name>$(P)$(R)DetuningCh$(CHAN)-RB</pv_name>
      <x>540</x>
      <y>120</y>
      <width>80</width>
      <height>30</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Detu2_label</name>
      <text>Detuning:</text>
      <x>460</x>
      <y>120</y>
      <width>70</width>
      <height>30</height>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>CavityAmp_label</name>
      <text>Cavity Amp:</text>
      <x>240</x>
      <y>200</y>
      <width>90</width>
      <height>30</height>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>CavityAmp_update</name>
      <pv_name>$(P)$(R)CalcAvgCavityAmpCh$(CHAN)-RB</pv_name>
      <x>340</x>
      <y>200</y>
      <width>80</width>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Piezo_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)ConfigurePiezoCh$(CHAN)-SP.PROC</pv_name>
      <text>Configure Piezo</text>
      <x>50</x>
      <y>340</y>
      <width>130</width>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>LLRF_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)ConfigureLLRF-SP.PROC</pv_name>
      <text>Configure LLRF</text>
      <x>270</x>
      <y>260</y>
      <width>130</width>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Restore_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)BringBackPiezoCh$(CHAN)-SP.PROC</pv_name>
      <text>Restore Piezo</text>
      <x>50</x>
      <y>380</y>
      <width>130</width>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Restore_button_1</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)BringBackLLRFParams-SP.PROC</pv_name>
      <text>Restore LLRF</text>
      <x>270</x>
      <y>300</y>
      <width>130</width>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button</name>
      <actions>
        <action type="open_display">
          <file>ramp.bob</file>
          <target>tab</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>Ramp Configuration</text>
      <x>480</x>
      <y>390</y>
      <width>150</width>
      <height>40</height>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>Piezo Identification</text>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>370</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 1" family="Liberation Sans" style="BOLD" size="22.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="Text" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
</display>
