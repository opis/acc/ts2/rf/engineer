from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import jarray  # noqa: F401
import csv


def readPVs():
    path = str(PVUtil.getString(pvs[0]))  # noqa: F821
    name = str(PVUtil.getString(pvs[1]))  # noqa: F821
    if path == "0":
        path = ""
    if name == "0":
        name = "data.csv"
    file = {"path": path, "name": name}

    detuUp = PVUtil.getDoubleArray(pvs[2])  # noqa: F821
    voltUp = PVUtil.getDoubleArray(pvs[3])  # noqa: F821
    detuDown = PVUtil.getDoubleArray(pvs[4])  # noqa: F821
    voltDown = PVUtil.getDoubleArray(pvs[5])  # noqa: F821
    approx = PVUtil.getDoubleArray(pvs[6])  # noqa: F821
    return file, [detuUp, voltUp, detuDown, voltDown, approx]


def toRows(data):
    lists = []
    for d in data:
        lists.append(d.tolist())
    rows = zip(*lists)
    return rows


def getFileName(file):
    name = file["name"]
    if not name:
        return
    if not file["path"]:
        return
    if ".csv" not in name:
        name = file["name"] + ".csv"
    return file["path"] + "/" + name


def saveCSV(rows, path):
    try:
        if not path:
            raise ValueError("empty path string")
        spamWriter = csv.writer(open(path, "a"), delimiter=";", lineterminator="\n")
        spamWriter.writerow(["Detu up", "Volt up", "Detu down", "Volt down", "Approx"])
        for x in rows:
            spamWriter.writerow(x)
        msg = "File saved successfully: " + path
        ScriptUtil.showMessageDialog(widget, msg)  # noqa: F821
        spamWriter.close()
    except IOError as d:
        ScriptUtil.getLogger().severe("Error saving to file")
        ScriptUtil.getLogger().severe(d.strerror)
    except ValueError as v:
        ScriptUtil.getLogger().severe("Error saving to file")
        ScriptUtil.getLogger().severe(v.message)
    except Exception as e:
        ScriptUtil.getLogger().severe("Error saving to file")
        ScriptUtil.getLogger().severe(e)


def main():
    file, data = readPVs()
    rows = toRows(data)
    p = getFileName(file)
    saveCSV(rows, p)


if __name__ == "__main__":
    main()
